package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/helper"
	"github.com/golang_api/backend/service"
)

type UserController interface {
	UpdateUser(ctx *gin.Context)
	ProfileUser(ctx *gin.Context)
}

type userController struct {
	userService service.UserSevice
	jwtService  service.JWTService
}

func NewUserController(userService service.UserSevice, jwtService service.JWTService) UserController {
	return &userController{
		userService: userService,
		jwtService:  jwtService,
	}
}

func (u *userController) UpdateUser(ctx *gin.Context) {
	var userUpdateDto dto.UserUpdateDTO
	errDto := ctx.ShouldBind(&userUpdateDto)
	if errDto != nil {
		response := helper.BuildErrorResponse("Failed to process", errDto.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	token, errToken := u.jwtService.ValidateToken(authHeader)

	if errToken != nil {
		panic(errToken.Error())
	}

	claims := token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])

	userUpdateDto.ID = id

	data := u.userService.UpdateUser(userUpdateDto)
	response := helper.BuildResponse(true, "Success Update", data)
	ctx.JSON(http.StatusOK, response)
}

func (u *userController) ProfileUser(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	token, errToken := u.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}

	claims := token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	user := u.userService.ProfileUser(id)
	response := helper.BuildResponse(true, "profiel user", user)
	ctx.JSON(http.StatusOK, response)
}
