package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/entity"
	"github.com/golang_api/backend/helper"
	"github.com/golang_api/backend/service"
)

type BookController interface {
	All(context *gin.Context)
	FindById(context *gin.Context)
	Insert(context *gin.Context)
	Update(context *gin.Context)
	Delete(context *gin.Context)
}

type bookController struct {
	bookService service.BookService
	jwtService  service.JWTService
}

func NewBookController(bookServ service.BookService, jwtServ service.JWTService) BookController {
	return &bookController{
		bookService: bookServ,
		jwtService:  jwtServ,
	}
}

func (b *bookController) All(context *gin.Context) {
	var books []entity.Book = b.bookService.All()

	res := helper.BuildResponse(true, "retrieve all books", books)

	context.JSON(http.StatusOK, res)
}

func (c *bookController) FindById(context *gin.Context) {
	id := context.Param("id")

	var book entity.Book = c.bookService.FindByID(id)

	if (book == entity.Book{}) {
		res := helper.BuildErrorResponse("Book Not Found", "no data can retrieve", helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusNotFound, res)
	} else {
		res := helper.BuildResponse(true, "retrieve book", book)
		context.JSON(http.StatusOK, res)
	}
}

func (c *bookController) Insert(context *gin.Context) {
	var bookCreate dto.BookCreateDTO
	errDto := context.ShouldBind(&bookCreate)

	if errDto != nil {
		res := helper.BuildErrorResponse("Failed process", errDto.Error(), helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
	} else {
		authHeader := context.GetHeader("Authorization")
		userID := c.getUserIDByToken(authHeader)

		bookCreate.UserID = userID

		result := c.bookService.Insert(bookCreate)
		res := helper.BuildResponse(true, "success save book", result)
		context.JSON(http.StatusCreated, res)
	}
}

func (c *bookController) Update(context *gin.Context) {

	var bookUpdate dto.BookUpdateDTO

	errorDto := context.ShouldBind(&bookUpdate)
	if errorDto != nil {
		res := helper.BuildErrorResponse("Failed process", errorDto.Error(), helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	authHeader := context.GetHeader("Authorization")

	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}

	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprint("", claims["user_id"])
	if c.bookService.IsAllowedToEdit(bookUpdate.ID, userID) {
		bookUpdate.UserID = userID

		result := c.bookService.Update(bookUpdate)
		response := helper.BuildResponse(true, "book updated", result)

		context.JSON(http.StatusOK, response)
	} else {
		response := helper.BuildErrorResponse("Not allowed to update", "You are not the owner", helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusForbidden, response)
	}

}

func (c *bookController) Delete(context *gin.Context) {

	var book entity.Book

	id := context.Param("id")

	book.ID = id
	authHeader := context.GetHeader("Authorization")

	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}

	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprint("", claims["user_id"])
	if c.bookService.IsAllowedToEdit(book.ID, userID) {
		c.bookService.Delete(book)
		res := helper.BuildResponse(true, "book deleted", helper.EmptyObj{})
		context.JSON(http.StatusOK, res)
	} else {
		res := helper.BuildErrorResponse("Not allowed to update", "You are not the owner", helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusForbidden, res)
	}

}
func (b *bookController) getUserIDByToken(token string) string {

	aToken, err := b.jwtService.ValidateToken(token)

	if err != nil {
		panic(err.Error())
	}

	claims := aToken.Claims.(jwt.MapClaims)
	id := fmt.Sprint("", claims["user_id"])
	return id
}
