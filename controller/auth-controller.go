package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/entity"
	"github.com/golang_api/backend/helper"
	"github.com/golang_api/backend/service"
)

// interface is contract what this controller can do
type AuthController interface {
	Login(ctx *gin.Context)
	Register(ctx *gin.Context)
}

type authController struct {
	authService service.AuthService
	jwtService  service.JWTService
}

func NewAuthController(auth service.AuthService, jwt service.JWTService) AuthController {
	return &authController{
		authService: auth,
		jwtService:  jwt,
	}
}

func (c *authController) Login(ctx *gin.Context) {
	var loginDto dto.LoginDTO

	errDto := ctx.ShouldBind(&loginDto)
	if errDto != nil {
		response := helper.BuildErrorResponse("Failed to process", errDto.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	authResult := c.authService.VerifyCredential(loginDto.Email, loginDto.Password)
	if v, ok := authResult.(entity.User); ok {
		generatedToken := c.jwtService.GenerateToken(v.ID)
		v.Token = generatedToken
		response := helper.BuildResponse(true, "OK!", v)
		ctx.JSON(http.StatusOK, response)
		return
	}

	response := helper.BuildErrorResponse("Please check your credential", "Invalid credential", helper.EmptyObj{})
	ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
}

func (c *authController) Register(ctx *gin.Context) {
	var registerDto dto.RegisterDTO

	errDto := ctx.ShouldBind(&registerDto)

	if errDto != nil {
		response := helper.BuildErrorResponse("Failed to process", errDto.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if !c.authService.IsDuplicateEmail(registerDto.Email) {
		response := helper.BuildErrorResponse("Failed to process", "Email already exits", helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		createdUser := c.authService.CreateUser(registerDto)
		token := c.jwtService.GenerateToken(createdUser.ID)
		createdUser.Token = token
		response := helper.BuildResponse(true, "success", createdUser)
		ctx.JSON(http.StatusOK, response)
	}
}
