package config

import (
	"fmt"
	"os"

	"github.com/golang_api/backend/entity"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// new connection database
func SetuDatababaseConnection() *gorm.DB {
	errEnv := godotenv.Load()

	if errEnv != nil {
		panic("Failed to load env")
	}

	dbUser := os.Getenv("DB_USERNAME")
	dbPass := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_DATABASE")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbName)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed connect to your database")
	}

	db.AutoMigrate(&entity.User{}, &entity.Book{})

	return db
}

// close database
func CloseConnectionDatabase(db *gorm.DB) {
	dbsql, err := db.DB()

	if err != nil {
		panic("Failed to close connection from database")
	}
	dbsql.Close()
}
