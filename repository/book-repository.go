package repository

import (
	"github.com/golang_api/backend/entity"
	"gorm.io/gorm"
)

type BookRepository interface {
	InsertBook(book entity.Book) entity.Book
	UpdateBook(book entity.Book) entity.Book
	DeleteBook(book entity.Book)
	AllBook() []entity.Book
	FindBook(id string) entity.Book
}

type bookRepository struct {
	bookConnection *gorm.DB
}

func NewBookRepository(db *gorm.DB) BookRepository {
	return &bookRepository{
		bookConnection: db,
	}
}

func (b *bookRepository) InsertBook(book entity.Book) entity.Book {
	b.bookConnection.Save(&book)
	b.bookConnection.Preload("User").Find(&book)

	return book
}

func (b *bookRepository) UpdateBook(book entity.Book) entity.Book {
	b.bookConnection.Save(&book)
	b.bookConnection.Preload("User").Find(&book)

	return book
}

func (b *bookRepository) DeleteBook(book entity.Book) {
	b.bookConnection.Delete(&book)
}

func (b *bookRepository) FindBook(id string) entity.Book {
	var book entity.Book

	b.bookConnection.Preload("User").Find(&book, "id = ?", id)

	return book
}

func (b *bookRepository) AllBook() []entity.Book {
	var books []entity.Book

	b.bookConnection.Preload("User").Find(&books)

	return books
}
