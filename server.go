package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/golang_api/backend/config"
	"github.com/golang_api/backend/controller"
	"github.com/golang_api/backend/middleware"
	"github.com/golang_api/backend/repository"
	"github.com/golang_api/backend/service"
	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.SetuDatababaseConnection()

	//repository
	userRepo repository.UserRepository = repository.NewUserRepository(db)
	bookRepo repository.BookRepository = repository.NewBookRepository(db)
	//service
	authService service.AuthService = service.NewAuthService(userRepo)
	jwtService  service.JWTService  = service.NewJWTService()
	userService service.UserSevice  = service.NewUserService(userRepo)
	bookService service.BookService = service.NewBookService(bookRepo)
	//controller
	authController controller.AuthController = controller.NewAuthController(authService, jwtService)
	userController controller.UserController = controller.NewUserController(userService, jwtService)
	bookController controller.BookController = controller.NewBookController(bookService, jwtService)
)

func main() {
	defer config.CloseConnectionDatabase(db)

	router := gin.Default()

	authRoutes := router.Group("api/auth")
	{
		authRoutes.POST("/login", authController.Login)
		authRoutes.POST("/register", authController.Register)
	}

	userRoutes := router.Group("api/user", middleware.AuthorizeJWT(jwtService))
	{
		userRoutes.GET("/profile", userController.ProfileUser)
		userRoutes.PUT("/profile", userController.UpdateUser)
	}

	bookRoutes := router.Group("api/books", middleware.AuthorizeJWT(jwtService))
	{
		bookRoutes.GET("/", bookController.All)
		bookRoutes.POST("/", bookController.Insert)
		bookRoutes.GET("/:id", bookController.FindById)
		bookRoutes.PUT("/:id", bookController.Update)
		bookRoutes.DELETE("/:id", bookController.Delete)
	}

	router.Use(cors.Default())

	router.Run()
}
