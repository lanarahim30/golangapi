package dto

type UserUpdateDTO struct {
	ID       string `json:"id" form:"id" binding:"required"`
	Name     string `json:"name" form:"name" binding:"required,max=255"`
	Email    string `json:"email" form:"email" binding:"required,email,max=255" `
	Phone    string `json:"phone" form:"phone,omitempty" binding:"max=13"`
	Password string `json:"password" form:"password,omitempty" binding:"max=10"`
	Birthday string `json:"birthday" form:"birthday,omitempty"`
	Status   bool   `json:"status" form:"status"`
}
