package service

import (
	"log"

	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/entity"
	"github.com/golang_api/backend/repository"
	"github.com/mashingan/smapping"
)

type UserSevice interface {
	UpdateUser(user dto.UserUpdateDTO) entity.User
	ProfileUser(UserID string) entity.User
}

type userService struct {
	userRepo repository.UserRepository
}

func NewUserService(userRepo repository.UserRepository) UserSevice {
	return &userService{
		userRepo: userRepo,
	}
}

func (u *userService) UpdateUser(user dto.UserUpdateDTO) entity.User {
	userUpdate := entity.User{}

	err := smapping.FillStruct(&userUpdate, smapping.MapFields(user))

	if err != nil {
		log.Fatal("Failed map %v", err)
	}
	updateUser := u.userRepo.UpdateUser(userUpdate)

	return updateUser
}

func (u *userService) ProfileUser(UserID string) entity.User {
	return u.userRepo.ProfileUser(UserID)
}
