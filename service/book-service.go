package service

import (
	"fmt"
	"log"

	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/entity"
	"github.com/golang_api/backend/repository"
	"github.com/mashingan/smapping"
)

type BookService interface {
	Insert(book dto.BookCreateDTO) entity.Book
	Update(book dto.BookUpdateDTO) entity.Book
	Delete(book entity.Book)
	FindByID(id string) entity.Book
	All() []entity.Book
	IsAllowedToEdit(id string, userId string) bool
}

type bookService struct {
	bookRepo repository.BookRepository
}

func NewBookService(bookRepo repository.BookRepository) BookService {
	return &bookService{
		bookRepo: bookRepo,
	}
}

func (service *bookService) Insert(book dto.BookCreateDTO) entity.Book {
	bookCreate := entity.Book{}

	err := smapping.FillStruct(&bookCreate, smapping.MapFields(&book))

	if err != nil {
		log.Fatal("Failed Map %v", err)
	}

	res := service.bookRepo.InsertBook(bookCreate)

	return res
}

func (service *bookService) Update(book dto.BookUpdateDTO) entity.Book {
	bookCreate := entity.Book{}
	err := smapping.FillStruct(&bookCreate, smapping.MapFields(&book))

	if err != nil {
		log.Fatal("Failed Map %v", err)
	}

	res := service.bookRepo.UpdateBook(bookCreate)

	return res
}

func (service *bookService) Delete(book entity.Book) {
	service.bookRepo.DeleteBook(book)
}

func (service *bookService) All() []entity.Book {
	return service.bookRepo.AllBook()
}

func (service *bookService) FindByID(id string) entity.Book {

	return service.bookRepo.FindBook(id)
}

func (service *bookService) IsAllowedToEdit(id string, UserID string) bool {

	b := service.bookRepo.FindBook(id)

	bookUserID := fmt.Sprint("", b.UserID)

	return bookUserID == UserID
}
