package service

import (
	"log"

	"github.com/golang_api/backend/dto"
	"github.com/golang_api/backend/entity"
	"github.com/golang_api/backend/repository"
	"github.com/mashingan/smapping"
	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	VerifyCredential(email string, password string) interface{}
	CreateUser(user dto.RegisterDTO) entity.User
	FindByEmail(email string) entity.User
	IsDuplicateEmail(email string) bool
}

type authService struct {
	userRepo repository.UserRepository
}

func NewAuthService(userRepo repository.UserRepository) AuthService {

	return &authService{
		userRepo: userRepo,
	}
}

func (a *authService) VerifyCredential(email string, password string) interface{} {
	res := a.userRepo.VerifyCredential(email, password)

	if v, ok := res.(entity.User); ok {
		comparedPassword := ComparePassword(v.Password, []byte(password))
		if v.Email == email && comparedPassword {
			return res
		}
		return false
	}
	return res
}

func (a *authService) CreateUser(user dto.RegisterDTO) entity.User {
	userToCreate := entity.User{}
	err := smapping.FillStruct(&userToCreate, smapping.MapFields(&user))

	if err != nil {
		log.Fatalf("Failed map %v", err)
	}
	res := a.userRepo.InsertUser(userToCreate)

	return res
}

func (a *authService) FindByEmail(email string) entity.User {
	return a.userRepo.FindByEmail(email)
}

func (a *authService) IsDuplicateEmail(email string) bool {
	res := a.userRepo.IsDuplicateEmail(email)

	return !(res.Error == nil)
}

func ComparePassword(hashPassword string, plainPassword []byte) bool {
	byteHas := []byte(hashPassword)
	err := bcrypt.CompareHashAndPassword(byteHas, plainPassword)

	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
