package entity

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// book table
type Book struct {
	ID          string         `gorm:"primaryKey" json:"id"`
	Title       string         `gorm:"type:varchar(255)" json:"title"`
	Description string         `gorm:"type:text" json:"description"`
	UserID      string         `gorm:"not null;size:191" json:"-"`
	User        User           `gorm:"foreignKey:UserID;constraint:onUpdate:CASCADE" json:"user"`
	CreatedAt   int64          `gorm:"autoCreateTime:milli"`
	UpdatedAt   int64          `gorm:"autoUpdateTime:milli"`
	DeletedAt   gorm.DeletedAt `gorm:"index"`
}

func (book *Book) BeforeCreate(tx *gorm.DB) (err error) {
	// UUID version 4
	book.ID = uuid.NewString()
	return
}
