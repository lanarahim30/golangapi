package entity

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// user table
type User struct {
	ID        string         `gorm:"primaryKey" json:"id"`
	Name      string         `gorm:"type:varchar(255)" json:"name"`
	Email     string         `gorm:"uniqueIndex;type:varchar(255)" json:"email"`
	Phone     string         `gorm:"uniqueIndex;type:varchar(13)" json:"phone"`
	Birthday  string         `json:"birthday"`
	Password  string         `gorm:"->;<-;not null" json:"-"`
	Status    bool           `gorm:"default:true" json:"status"`
	Token     string         `gorm:"-" json:"token,omitempty"`
	CreatedAt int64          `gorm:"autoCreateTime:milli" json:"created_at"`
	UpdatedAt int64          `gorm:"autoUpdateTime:milli" json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}

func (user *User) BeforeCreate(tx *gorm.DB) (err error) {
	// UUID version 4
	user.ID = uuid.NewString()
	return
}
